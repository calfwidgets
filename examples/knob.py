import pygtk
pygtk.require('2.0')
import gtk
import calfwidgets

def destroy(win):
    gtk.main_quit()

window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.connect("destroy", destroy)

vbox = gtk.VBox()
for mode in (calfwidgets.KNOB_MODE_POSITIVE, calfwidgets.KNOB_MODE_BIPOLAR, calfwidgets.KNOB_MODE_NEGATIVE, calfwidgets.KNOB_MODE_ENDLESS):
    knob = calfwidgets.Knob()
    knob.set_adjustment(gtk.Adjustment(0.5, 0, 1, 0.1, 0.1, 0))
    knob.set_mode(mode)
    vbox.add(knob)
window.add(vbox)
window.show_all()

gtk.main()
