import pygtk
pygtk.require('2.0')
import gtk
import calfwidgets

def destroy(win):
    gtk.main_quit()

window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.connect("destroy", destroy)

hbox = gtk.HBox()
for m in (calfwidgets.VU_METER_STANDARD, calfwidgets.VU_METER_MONOCHROME, calfwidgets.VU_METER_MONOCHROME_REVERSE):
    if m != calfwidgets.VU_METER_STANDARD:
        hbox.pack_start(gtk.VSeparator(), padding = 3)
    vbox = gtk.VBox()
    for i in range(0, 11):
        meter = calfwidgets.VUMeter()
        meter.set_value(i / 10.0)
        meter.set_mode(m)
        vbox.add(meter)
    hbox.add(vbox)
window.add(hbox)
window.show_all()

gtk.main()
