import pygtk
pygtk.require('2.0')
import gtk
import calfwidgets

def destroy(win):
    gtk.main_quit()

window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.connect("destroy", destroy)

vbox = gtk.VBox()
led1 = calfwidgets.Led()
vbox.add(led1)
led2 = calfwidgets.Led()
led2.set_state(1)
vbox.add(led2)
window.add(vbox)
window.show_all()

gtk.main()
