/* Calf DSP Library
 * A bitmap-based knob with several variants
 *
 * Copyright (C) 2007-2008 Krzysztof Foltman
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __CALF_KNOB_CTL
#define __CALF_KNOB_CTL

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CALF_TYPE_KNOB          (calf_knob_get_type())
#define CALF_KNOB(obj)          (G_TYPE_CHECK_INSTANCE_CAST ((obj), CALF_TYPE_KNOB, CalfKnob))
#define CALF_IS_KNOB(obj)       (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CALF_TYPE_KNOB))
#define CALF_KNOB_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass),  CALF_TYPE_KNOB, CalfKnobClass))
#define CALF_IS_KNOB_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CALF_TYPE_KNOB))

/** Knob mode (affects operation and bitmap used) */
typedef enum 
{
    CALF_KNOB_MODE_POSITIVE, /**< positive - values above minimum are increasingly "hotter" */
    CALF_KNOB_MODE_BIPOLAR, /**< bipolar - hot values are between middle and current knob setting */
    CALF_KNOB_MODE_NEGATIVE, /**< negative - hot values are between current knob setting and max */
    CALF_KNOB_MODE_ENDLESS /**< endless (modulo) - for phase settings etc. */
} CalfKnobMode;

/** Object instance struct for CalfKnob */
struct _CalfKnob
{
    GtkRange parent;
    CalfKnobMode mode;
    double start_x, start_y, start_value, last_y;
};

/** Class object struct for CalfKnob */
struct _CalfKnobClass
{
    GtkRangeClass parent_class;
    GdkPixbuf *knob_image;
};

typedef struct _CalfKnob CalfKnob;
typedef struct _CalfKnobClass CalfKnobClass;

/** Create a knob widget */
GtkWidget *calf_knob_new();

/** Create a knob widget and set a GtkAdjustment */
GtkWidget *calf_knob_new_with_adjustment (GtkAdjustment *_adjustment);

/** Set knob mode (positive, negative, bipolar, endless) */
void calf_knob_set_mode (CalfKnob *knob, CalfKnobMode mode);

/** Get knob mode (positive, negative, bipolar, endless) */
CalfKnobMode calf_knob_get_mode (CalfKnob *knob);

/** Get GObject type for CalfKnob */
GType calf_knob_get_type();

G_END_DECLS

#endif
