/* Calf DSP Library
 * Line graph custom control - communication interface
 * Copyright (C) 2007-2008 Krzysztof Foltman
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __CALF_LINEGRAPH_IFACE_H
#define __CALF_LINEGRAPH_IFACE_H

struct _cairo;
    
typedef struct _cairo cairo_t;

/// 'provides live line graph values' interface
struct line_graph_iface
{
    /// Obtain subindex'th graph of parameter index
    /// @param index parameter/graph number (usually tied to particular plugin control port)
    /// @param subindex graph number (there may be multiple overlaid graphs for one parameter, eg. for monosynth 2x12dB filters)
    /// @param data buffer for normalized output values
    /// @param points number of points to fill
    /// @param context cairo context to adjust (for multicolour graphs etc.)
    /// @retval true graph data was returned; subindex+1 graph may or may not be available
    /// @retval false graph data was not returned; subindex+1 graph does not exist either
    virtual bool get_graph(int index, int subindex, float *data, int points, cairo_t *context) { return false; }
    
    /// Obtain subindex'th static graph of parameter index (static graphs are only dependent on parameter value, not plugin state)
    /// @param index parameter/graph number (usually tied to particular plugin control port)
    /// @param subindex graph number (there may be multiple overlaid graphs for one parameter, eg. for monosynth 2x12dB filters)
    /// @param value parameter value to pick the graph for
    /// @param data buffer for normalized output values
    /// @param points number of points to fill
    /// @param context cairo context to adjust (for multicolour graphs etc.)
    /// @retval true graph data was returned; subindex+1 graph may or may not be available
    /// @retval false graph data was not returned; subindex+1 graph does not exist either
    virtual bool get_static_graph(int index, int subindex, float value, float *data, int points, cairo_t *context) { return false; }
    /// Obtain subindex'th dot of parameter 'index'
    /// @param index parameter/dot number (usually tied to particular plugin control port)
    /// @param subindex dot number (there may be multiple dots graphs for one parameter)
    virtual bool get_dot(int index, int subindex, float &x, float &y, int &size, cairo_t *context) { return false; }
    /// Standard destructor to make compiler happy
    virtual ~line_graph_iface() {}
};

#endif
