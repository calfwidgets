/* Calf DSP Library
 * A LED-line-style volume meter widget
 *
 * Copyright (C) 2007-2008 Krzysztof Foltman
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __CALF_VUMETER_CTL
#define __CALF_VUMETER_CTL

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CALF_TYPE_VU_METER          (calf_vu_meter_get_type())
#define CALF_VU_METER(obj)          (G_TYPE_CHECK_INSTANCE_CAST ((obj), CALF_TYPE_VU_METER, CalfVUMeter))
#define CALF_IS_VU_METER(obj)       (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CALF_TYPE_VU_METER))
#define CALF_VU_METER_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass),  CALF_TYPE_VU_METER, CalfVUMeterClass))
#define CALF_IS_VU_METER_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CALF_TYPE_VU_METER))

typedef enum 
{
    CALF_VU_METER_MODE_STANDARD,
    CALF_VU_METER_MODE_MONOCHROME,
    CALF_VU_METER_MODE_MONOCHROME_REVERSE
} CalfVUMeterMode;

struct _CalfVUMeter
{
    GtkWidget parent;
    CalfVUMeterMode mode;
    float value;
};

struct _CalfVUMeterClass
{
    GtkWidgetClass parent_class;
};

typedef struct _CalfVUMeter CalfVUMeter;
typedef struct _CalfVUMeterClass CalfVUMeterClass;

GType calf_vu_meter_get_type();

GtkWidget *calf_vu_meter_new ();
void calf_vu_meter_set_value (CalfVUMeter *meter, float value);
float calf_vu_meter_get_value (CalfVUMeter *meter);
void calf_vu_meter_set_mode (CalfVUMeter *meter, CalfVUMeterMode mode);
CalfVUMeterMode calf_vu_meter_get_mode (CalfVUMeter *meter);

G_END_DECLS

#endif
