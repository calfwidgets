/* Calf DSP Library
 * A LED-line-style volume meter widget
 * Copyright (C) 2007-2008 Krzysztof Foltman
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <calfwidgets/vumeter.h>
#include <cairo/cairo.h>
#include <math.h>
#include <stdint.h>
#include <malloc.h>

///////////////////////////////////////// vu meter ///////////////////////////////////////////////

static gboolean
calf_vu_meter_expose (GtkWidget *widget, GdkEventExpose *event)
{
    g_assert(CALF_IS_VU_METER(widget));
    
    CalfVUMeter *vu = CALF_VU_METER(widget);
    int ox = widget->allocation.x + 1, oy = widget->allocation.y + 1;
    int sx = widget->allocation.width - 2, sy = widget->allocation.height - 2;
    
    cairo_t *c = gdk_cairo_create(GDK_DRAWABLE(widget->window));

    GdkColor sc = { 0, 0, 0, 0 };
    gdk_cairo_set_source_color(c, &sc);
    cairo_rectangle(c, ox, oy, sx, sy);
    cairo_fill(c);
    cairo_set_line_width(c, 1);
    
    CalfVUMeterMode mode = vu->mode;
    
    for (int x = ox; x <= ox + sx; x += 3)
    {
        float ts = (x - ox) * 1.0 / sx;
        float r = 0.f, g = 0.f, b = 0.f;
        switch(mode)
        {
            case CALF_VU_METER_MODE_STANDARD:
            default:
                if (ts < 0.75)
                    r = ts / 0.75, g = 1, b = 0;
                else
                    r = 1, g = 1 - (ts - 0.75) / 0.25, b = 0;
                if (vu->value < ts || vu->value <= 0)
                    r *= 0.5, g *= 0.5, b *= 0.5;
                break;
            case CALF_VU_METER_MODE_MONOCHROME_REVERSE:
                r = 1, g = 1, b = 0;
                if (!(vu->value < ts || vu->value <= 0))
                    r *= 0.5, g *= 0.5, b *= 0.5;
                break;
            case CALF_VU_METER_MODE_MONOCHROME:
                r = 1, g = 1, b = 0;
                if (vu->value < ts || vu->value <= 0)
                    r *= 0.5, g *= 0.5, b *= 0.5;
                break;
        }
        GdkColor sc2 = { 0, (guint16)(65535 * r), (guint16)(65535 * g), (guint16)(65535 * b) };
        gdk_cairo_set_source_color(c, &sc2);
        cairo_move_to(c, x, oy);
        cairo_line_to(c, x, oy + sy + 1);
        cairo_move_to(c, x, oy + sy);
        cairo_line_to(c, x, oy );
        cairo_stroke(c);
    }

    cairo_destroy(c);
    
    gtk_paint_shadow(widget->style, widget->window, GTK_STATE_NORMAL, GTK_SHADOW_IN, NULL, widget, NULL, ox - 1, oy - 1, sx + 2, sy + 2);
    // printf("exposed %p %d+%d\n", widget->window, widget->allocation.x, widget->allocation.y);
    
    return TRUE;
}

static void
calf_vu_meter_size_request (GtkWidget *widget,
                           GtkRequisition *requisition)
{
    g_assert(CALF_IS_VU_METER(widget));
    
    requisition->width = 50;
    requisition->height = 14;
}

static void
calf_vu_meter_size_allocate (GtkWidget *widget,
                           GtkAllocation *allocation)
{
    g_assert(CALF_IS_VU_METER(widget));
    
    widget->allocation = *allocation;
}

static void
calf_vu_meter_class_init (CalfVUMeterClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
    widget_class->expose_event = calf_vu_meter_expose;
    widget_class->size_request = calf_vu_meter_size_request;
    widget_class->size_allocate = calf_vu_meter_size_allocate;
}

static void
calf_vu_meter_init (CalfVUMeter *self)
{
    GtkWidget *widget = GTK_WIDGET(self);
    GTK_WIDGET_SET_FLAGS (widget, GTK_NO_WINDOW);
    widget->requisition.width = 40;
    widget->requisition.height = 40;
    self->value = 0;
}

GtkWidget *
calf_vu_meter_new()
{
    return GTK_WIDGET( g_object_new (CALF_TYPE_VU_METER, NULL ));
}

GType
calf_vu_meter_get_type (void)
{
    static GType type = 0;
    if (!type) {
        static const GTypeInfo type_info = {
            sizeof(CalfVUMeterClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            (GClassInitFunc)calf_vu_meter_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof(CalfVUMeter),
            0,    /* n_preallocs */
            (GInstanceInitFunc)calf_vu_meter_init
        };

        GTypeInfo *type_info_copy = new GTypeInfo(type_info);

        for (int i = 0; ; i++) {
            char *name = g_strdup_printf("CalfVUMeter%u%d", ((unsigned int)(intptr_t)calf_vu_meter_class_init) >> 16, i);
            if (g_type_from_name(name)) {
                free(name);
                continue;
            }
            type = g_type_register_static( GTK_TYPE_WIDGET,
                                           name,
                                           type_info_copy,
                                           (GTypeFlags)0);
            free(name);
            break;
        }
    }
    return type;
}

extern void calf_vu_meter_set_value(CalfVUMeter *meter, float value)
{
    if (value != meter->value)
    {
        meter->value = value;
        gtk_widget_queue_draw(GTK_WIDGET(meter));
    }
}

extern float calf_vu_meter_get_value(CalfVUMeter *meter)
{
    return meter->value;
}

extern void calf_vu_meter_set_mode(CalfVUMeter *meter, CalfVUMeterMode mode)
{
    if (mode != meter->mode)
    {
        meter->mode = mode;
        gtk_widget_queue_draw(GTK_WIDGET(meter));
    }
}

extern CalfVUMeterMode calf_vu_meter_get_mode(CalfVUMeter *meter)
{
    return meter->mode;
}

